import cv2
import numpy as np
import os
import sys
import math
from PIL import Image  
import PIL  
from matplotlib import pyplot
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from sklearn.utils import shuffle
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from numpy import savetxt,loadtxt
from tensorflow.keras.applications.resnet50 import preprocess_input, decode_predictions


print(len(tf.config.experimental.list_physical_devices('GPU')))
#augment data

class DataEntry:
    def __init__(self,imageName,isTumour):
        self.imageName = imageName
        self.isTumour = isTumour

        

class ImageLabel:
    def __init__(self,imageName,image):
        self.imageName = imageName
        self.image = image
        
def prepDataset():
    file = open("ISIC-2017_Test_v2_Part3_GroundTruth.csv","r")
    fileRead = file.read().splitlines()
    data = []
    count = 0
    tCount = 0
    nCount = 0
    for lines in fileRead:
        words = lines.split(",")
        isTumour = 0
        if words[1] == "1.0":
            isTumour = 1
            tCount += 1
        elif words[2] == "1.0":
            isTumour = 0
            nCount += 1
        else:
            isTumour = 0
            nCount += 1

        
        entry = DataEntry(words[0],isTumour)
        if count!=0:
            data.append(entry)
        count = count + 1
    print(len(data))
    print(tCount)
    print(nCount)
    
    return data

def prepTrainImages():
    imageArray = []
    nameArray = []
    imageLabelA = []
    trainImages = []
    trainLabels = []
     
    labels = prepDataset()
    
    
    
    count = 0
    for label in labels:
        print(count/len(labels)*100)
        print("%")
        count = count + 1
        string = "ISIC-2017_Test_v2_Data/" + label.imageName + ".jpg"
        image = cv2.imread(string,cv2.IMREAD_COLOR)
        newImage = cv2.resize(image,(300,400))
        #image = hairRemoval(image)
        newImage = showImage(newImage)
        newImage = cv2.resize(image,(75,100))
        imageArray.append(newImage)
        trainLabels.append(label.isTumour)

    
    
    
   
                

        
    return imageArray,trainLabels

def labelImages():
    return
def resizeImg(img):
    return img


def showImage(img):
    #showsImage, imgNum = imgNum
    test = hairRemoval(img)
    gray = cv2.cvtColor(test,cv2.COLOR_BGR2GRAY)
    #cv2.imshow("test Image", test)
    median = cv2.medianBlur(gray, 9)
    ret, th = cv2.threshold(median,0,255,cv2.THRESH_OTSU) #thresholding


    #tests
    kernel = np.ones((14,14),np.uint8) #kernel
    #erosion = cv2.erode(th,kernel,iterations=3) #erodes image
    #dilation = cv2.dilate(erosion,kernel, iterations=3) #dilation
    #opening = cv2.morphologyEx(th,cv2.MORPH_OPEN,kernel) #erosion + dilation
    #morph = cv2.morphologyEx(th, cv2.MORPH_GRADIENT, kernel)
    
    
    
    #close
    close = cv2.morphologyEx(th,cv2.MORPH_CLOSE,kernel)
    
    #cv2.imshow("Original Image", img)
    #cv2.imshow("Median Image",median)
    #cv2.imshow("OTSU Image",th) #median threshold
    #cv2.imshow("Erode/Dilated Image", dilation)
    #cv2.imshow("mroph", morph)
    #cv2.imshow("close",close)
    #cv2.imshow("Open Image", opening)
    
    
    #finding contours
    closeInv = cv2.bitwise_not(close) #inverts image
    result = np.zeros_like(closeInv)
    overlayedMask = overlayMask(img, result,closeInv) #overlays mask over original image
    return overlayedMask


def overlayMask(img,result,closeInv):


    # finding contours
    contours, hierachy = cv2.findContours(closeInv, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # draws largest contour
    bContour = max(contours, key=cv2.contourArea)
    cv2.drawContours(result, [bContour], -1, (255, 255, 255), -1)
    # removing noise
    result = cv2.medianBlur(result, 9)
    result = cv2.medianBlur(result, 9)

    #cv2.imshow("overlay mask",result) #shows non rotated mask

    mask = cv2.bitwise_and(img, img, mask=result)
    return mask

    #cv2.imshow("overlay mask2 ",mask)
    


def loopDirectory(dir):
    directory = os.fsencode(dir)
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".jpg"):
            #sys.stdout.write(filename)
            continue
        else:
            continue
def expandImg(img):
    top = bottom = left = right = 1280
    dst = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, (0, 0, 0))
    #cv2.imshow("boxExpanded", dst)
    return dst


def sym(img,cnt):
    #analyses symmetry

    flip = 0
    rect = cv2.minAreaRect(cnt)
    center = rect[0]
    xyLen = rect[1]
    angle = rect[2]
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    rgb = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
    cv2.drawContours(rgb,[box],0,(0,0,0),2)
    cropped = cv2.getRectSubPix(rgb,(int(xyLen[0]),int(xyLen[1])),center)
    flipx = 0
    flipy = 0
    if angle<-45:
        angle= angle+90
        flipx = xyLen[1]
        flipy = xyLen[0]
    else:
        flipx =  xyLen[0]
        flipy = xyLen[1]


    flipM = cv2.getRotationMatrix2D(center, angle, 1)
    (h, w) = rgb.shape[:2]
    rotated = cv2.warpAffine(rgb, flipM, (w, h)) #rotates to straight
    croppedRot = cv2.getRectSubPix(rotated,(int(flipx),int(flipy)),center) #crops image

    #cv2.imshow("cropped", croppedRot) #dusplays cropped image

    croppedRot = cv2.cvtColor(croppedRot, cv2.COLOR_BGR2GRAY)

    #create clone
    croppedClone = croppedRot
    (h, w) = croppedClone.shape[:2]
    if w>h:
        flip = 0
    else:
        flip = 1
    flippedClone = cv2.flip(croppedClone,flip)
    a = cv2.bitwise_or(croppedRot,flippedClone) #Lesion union Symmetry mask
    fs = cv2.bitwise_xor(croppedRot,a) #false symmetry
    cv2.imshow("asym", fs)
    #sym = 1 - (fs/a)
    #Formula taken from "AUTOMATIC BOUNDARY DETECTION AND SYMMETRY CALCULATION IN DEMOSCOPY IMAGES OF SKIN LESIONS"
    aCount = cv2.countNonZero(a)#counts white pixels
    fsCount = cv2.countNonZero(fs)#counts white pixels
    symmetry = 1-(fsCount/aCount)

    return symmetry



    


def hairRemoval(img):

    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    #cv2.imshow("edges",gray)
    kernel = np.ones((10,10),np.uint8)
    blackhat = cv2.morphologyEx(gray,cv2.MORPH_BLACKHAT, kernel)
    #cv2.imshow("edges2",blackhat)
    ret, th = cv2.threshold(blackhat,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    #cv2.imshow("thresh",th)
    output = cv2.inpaint(img,th,1,cv2.INPAINT_TELEA)
    #cv2.imshow("lines thing",output)
    
    #output = hairRemoval(output)
    
    return output
    
    

def shape(img):
    #analyses shape
    print("hi")


flagE = 0 #if file exists
#showImage("0024308")
flagE = 0
if(os.path.exists("preppedData/testsetNormal/testData.npy")):
    flagE = 1
flagE = 0
if flagE == 0:
    trainingImg, trainingLabels = prepTrainImages()
    trainingLabels = np.array(trainingLabels)
    trainingImg2 = np.array(trainingImg)
    trainingImg2 = np.array(trainingImg2).reshape(-1,75,100,3)
    trainingImg2 = trainingImg2
    np.save('preppedData/testsetNormal/testData', trainingImg2)
    np.save('preppedData/testsetNormal/testLabel',trainingLabels)
else:
    trainingImg2 = np.load('preppedData/testsetNormal/testData.npy')
    trainingLabels = np.load('preppedData/testsetNormal/testLabel.npy')
    

datagen = ImageDataGenerator()
trainingImg3 = []
for image in trainingImg2:
    b = image.astype(float)/255
    trainingImg3.append(b)
trainingImg3 = np.array(trainingImg3)

datagen.fit(trainingImg3)

# prepare an iterators to scale images
trIterator = datagen.flow(trainingImg3, trainingLabels, batch_size=64)



print(len(trainingImg2))
new_model = tf.keras.models.load_model('saved/myModelOverlay')
#test_loss, test_acc = new_model.evaluate(trainingImg2, trainingLabels, verbose=1)
loss, acc = new_model.evaluate(trIterator, steps=len(trIterator), verbose=1)

count = 0
'''
for image in trainingImg2:
    trainingImg2[count] = image/255
    count = count + 1
    '''

predictions = new_model.predict(trainingImg3,batch_size = 64, verbose = 1)
predictionValues = []
for predicted in predictions:
    tick = 0
    count = 0
    for value in predicted:
        if tick == 1 and (1-(1-value))>0.5:
            print("error")
        elif (1-(1-value))>0.5:
            predictionValues.append(count)
            tick = 1
        count += 1
    if tick == 0:
        print("error")
        print(predicted)

predictionValues = np.array(predictionValues)
print(tf.math.confusion_matrix(trainingLabels,predictionValues))
print('Restored model, accuracy: {:5.2f}%'.format(100 * acc))
    
