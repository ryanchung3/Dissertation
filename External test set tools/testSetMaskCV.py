import cv2
import numpy as np
import os
import sys
import math
from PIL import Image  
import PIL  
from matplotlib import pyplot
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
import skimage
from scipy.stats import skew, entropy


#curvature : draw contour, measure line and curvature, concave or convex. changes in curvature, how often curvature above a threshold 
#(how sharp it is)
class DataEntry:
    def __init__(self,imageName,isTumour):
        self.imageName = imageName
        self.isTumour = isTumour
def showImage(imgNum):
    #showsImage, imgNum = imgNum
    img = cv2.imread(imgNum,cv2.IMREAD_COLOR ) #loads image
    img = cv2.resize(img,(600,450))
    test = hairRemoval(img)
    gray = cv2.cvtColor(test,cv2.COLOR_BGR2GRAY)
    #cv2.imshow("test Image", test)
    median = cv2.medianBlur(gray, 9)
    ret, th = cv2.threshold(median,0,255,cv2.THRESH_OTSU) #thresholding


    #tests
    kernel = np.ones((14,14),np.uint8) #kernel
    #erosion = cv2.erode(th,kernel,iterations=3) #erodes image
    #dilation = cv2.dilate(erosion,kernel, iterations=3) #dilation
    #opening = cv2.morphologyEx(th,cv2.MORPH_OPEN,kernel) #erosion + dilation
    #morph = cv2.morphologyEx(th, cv2.MORPH_GRADIENT, kernel)
    
    
    
    #close
    close = cv2.morphologyEx(th,cv2.MORPH_CLOSE,kernel)
    
    #cv2.imshow("Original Image", img)
    #cv2.imshow("Median Image",median)
    #cv2.imshow("OTSU Image",th) #median threshold
    #cv2.imshow("Erode/Dilated Image", dilation)
    #cv2.imshow("mroph", morph)
    #cv2.imshow("close",close)
    #cv2.imshow("Open Image", opening)
    
    
    #finding contours
    closeInv = cv2.bitwise_not(close) #inverts image
    result = np.zeros_like(closeInv) #test if needed
    
    
    
    #non expanded contour for shape manipulation
    overlayedMask = overlayMask(img, result,closeInv) #overlays mask over original image
    contoursOg,hierachyOg = cv2.findContours(closeInv, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    #draws largest contour
    ogBContour = max(contoursOg, key = cv2.contourArea)
    
    
    
    #image expanded contours
    result = expandImg(result) #expands image
    closeInv = expandImg(closeInv) #expands image
    contours,hierachy = cv2.findContours(closeInv, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    #draws largest contour
    bContour = max(contours, key = cv2.contourArea)
    cv2.drawContours(result,[bContour],-1,(255,255,255),-1)
    
    
    #removing noise
    result = cv2.medianBlur(result, 9)
    result = cv2.medianBlur(result, 9)
    
    
        
    
    symmetry = sym(result,bContour) # returns symmetry as a value between 0 and 1. The higher the value, the more symmetric
    homogeny,energy, correlation = colour(overlayedMask)
    area,perimeter, convex, ecen, compact,roundness,convexVal = shape(overlayedMask, ogBContour)
    
    
    return symmetry,homogeny,energy, correlation,area,perimeter, convex, ecen, compact,roundness,convexVal
    
    
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def overlayMask(img,result,closeInv):


    # finding contours
    contours, hierachy = cv2.findContours(closeInv, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # draws largest contour
    bContour = max(contours, key=cv2.contourArea)
    cv2.drawContours(result, [bContour], -1, (255, 255, 255), -1)
    # removing noise
    result = cv2.medianBlur(result, 9)
    result = cv2.medianBlur(result, 9)

    #cv2.imshow("overlay mask",result) #shows non rotated mask

    mask = cv2.bitwise_and(img, img, mask=result)
    return mask

    #cv2.imshow("overlay mask2 ",mask)
    
def prepDataset():
    file = open("ISIC-2017_Test_v2_Part3_GroundTruth.csv","r")
    fileRead = file.read().splitlines()
    data = []
    count = 0
    tCount = 0
    nCount = 0
    for lines in fileRead:
        words = lines.split(",")
        isTumour = 0
        if words[1] == "1.0":
            isTumour = 1
            tCount += 1
        elif words[2] == "1.0":
            isTumour = 0
            nCount += 1
        else:
            isTumour = 0
            nCount += 1

        
        entry = DataEntry(words[0],isTumour)
        if count!=0:
            data.append(entry)
        count = count + 1
    print(len(data))
    print(tCount)
    print(nCount)
    
    return data

def loopDirectory(dir):
    directory = os.fsencode(dir)
    count = 0
    symmetryList= []
    homogenyList= []
    energyList= []
    correlationList= []
    areaList = []
    perimeterList= []
    convexList= [] 
    ecenList= [] 
    compactList= []
    roundnessList= []
    convexValList= []
    ageList= []
    sexList = []
    labelList = []
    labels = prepDataset()
    for label in labels:
        print(count/len(labels)*100)
        print("%")
        count = count + 1
        string = "ISIC-2017_Test_v2_Data/" + label.imageName + ".jpg"
        image = cv2.imread(string,cv2.IMREAD_COLOR)
        if 1==1:
            symmetry,homogeny,energy, correlation,area,perimeter, convex, ecen, compact,roundness,convexVal = showImage(string)
            symmetryList.append(symmetry)
            homogenyList.append(homogeny)
            energyList.append(energy)
            correlationList.append(correlation)
            areaList .append(area)
            perimeterList.append(perimeter)
            convexList.append(convex)
            ecenList.append(ecen)
            compactList.append(compact)
            roundnessList.append(roundness)
            convexValList.append(convexVal)
            labelList.append(label.isTumour)
        else:
            continue
    
    return symmetryList,homogenyList,energyList, correlationList,areaList,perimeterList, convexList, ecenList, compactList,roundnessList,convexValList,ageList,sexList,labelList
def expandImg(img):
    top = bottom = 1280
    left = right = 1280
    dst = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, (0, 0, 0))
    #cv2.imshow("boxExpanded", dst)
    return dst


def sym(img,cnt):
    #analyses symmetry

    flip = 0
    rect = cv2.minAreaRect(cnt)
    center = rect[0]
    xyLen = rect[1]
    angle = rect[2]
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    rgb = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
    cv2.drawContours(rgb,[box],0,(0,0,0),2)
    cropped = cv2.getRectSubPix(rgb,(int(xyLen[0]),int(xyLen[1])),center)
    flipx = 0
    flipy = 0
    if angle<-45:
        angle= angle+90
        flipx = xyLen[1]
        flipy = xyLen[0]
    else:
        flipx =  xyLen[0]
        flipy = xyLen[1]


    flipM = cv2.getRotationMatrix2D(center, angle, 1)
    (h, w) = rgb.shape[:2]
    rotated = cv2.warpAffine(rgb, flipM, (w, h)) #rotates to straight
    croppedRot = cv2.getRectSubPix(rotated,(int(flipx),int(flipy)),center) #crops image

    #cv2.imshow("cropped", croppedRot) #dusplays cropped image

    croppedRot = cv2.cvtColor(croppedRot, cv2.COLOR_BGR2GRAY)

    #create clone
    croppedClone = croppedRot
    (h, w) = croppedClone.shape[:2]
    if w>h:
        flip = 0
    else:
        flip = 1
    flippedClone = cv2.flip(croppedClone,flip)
    a = cv2.bitwise_or(croppedRot,flippedClone) #Lesion union Symmetry mask
    fs = cv2.bitwise_xor(croppedRot,a) #false symmetry
    #cv2.imshow("asym", fs)
    #sym = 1 - (fs/a)
    #Formula taken from "AUTOMATIC BOUNDARY DETECTION AND SYMMETRY CALCULATION IN DEMOSCOPY IMAGES OF SKIN LESIONS"
    aCount = cv2.countNonZero(a)#counts white pixels
    fsCount = cv2.countNonZero(fs)#counts white pixels
    symmetry = 1-(fsCount/aCount)

    return symmetry



    


def hairRemoval(img):


    
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    #cv2.imshow("edges",gray)
    kernel = np.ones((10,10),np.uint8)
    blackhat = cv2.morphologyEx(gray,cv2.MORPH_BLACKHAT, kernel)
    #cv2.imshow("edges2",blackhat)
    ret, th = cv2.threshold(blackhat,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    #cv2.imshow("thresh",th)
    output = cv2.inpaint(img,th,1,cv2.INPAINT_TELEA)
    #cv2.imshow("lines thing",output)
    
    #output = hairRemoval(output)
    
    return output
    
def eccentricity(img,cnt):
    elon = 0
    flip = 0
    rect = cv2.minAreaRect(cnt)
    center = rect[0]
    xyLen = rect[1]
    angle = rect[2]
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    cv2.drawContours(img,[box],0,(0,0,0),2)
    cropped = cv2.getRectSubPix(img,(int(xyLen[0]),int(xyLen[1])),center)
    flipx = 0
    flipy = 0
    if angle<-45:
        angle= angle+90
        flipx = xyLen[1]
        flipy = xyLen[0]
        
    else:
        flipx =  xyLen[0]
        flipy = xyLen[1]


    flipM = cv2.getRotationMatrix2D(center, angle, 1)
    (h, w) = img.shape[:2]
    rotated = cv2.warpAffine(img, flipM, (w, h)) #rotates to straight
    croppedRot = cv2.getRectSubPix(rotated,(int(flipx),int(flipy)),center) #crops image
    (h,w) = croppedRot.shape[:2]
    
    
    #formula from https://www.microimages.com/documentation/TechGuides/81PolyShape.pdf
    if w>h:
        ecen = h/w
    else:
        ecen = w/h
    return ecen


def compactness(img,cnt):
    area = cv2.contourArea(cnt)
    
    
    perimeter = cv2.arcLength(cnt,True)
    
    sqrtPerimeter = perimeter**2
    
    piArea = (np.pi * 4)*area
    
    compactness = piArea/sqrtPerimeter #formula from http://www.cyto.purdue.edu/cdroms/micro2/content/education/wirth10.pdf
    return compactness
    
def distanceTwoPoints(p1,p2): 
    x1 = p1[0][0][0]
    y1 = p1[0][0][1]
    x2 = p2[0][0][0]
    y2 = p2[0][0][1]
    distance =((x1-x2)*(x1-x2))+ ((y1-y2)*(y1-y2))
    return distance

def convexity(img,cnt):
    hull_list = []
    
    for i in range(len(cnt)):
        hull = cv2.convexHull(cnt[i])
        hull_list.append(hull)
    perimeter = 0
    for i in range(len(hull_list)-1):
        perimeter = perimeter + distanceTwoPoints(hull_list[i],hull_list[i+1])
    return perimeter/cv2.arcLength(cnt,True)



def circularity(img, cnt):
    hull_list = []
    
    for i in range(len(cnt)):
        hull = cv2.convexHull(cnt[i])
        hull_list.append(hull)
    perimeter = 0
    for i in range(len(hull_list)-1):
        perimeter = perimeter + distanceTwoPoints(hull_list[i],hull_list[i+1])
    
    
    #formula from http://www.cyto.purdue.edu/cdroms/micro2/content/education/wirth10.pdf
    return  cv2.contourArea(cnt)/(perimeter**2/(np.pi*4))
    
        
    #to do: find distances between points and find perimeter
    
def shape(img,cnt):
    area = cv2.contourArea(cnt)
    area = area/480000 #so value is between 0-1
    perimeter = cv2.arcLength(cnt,True)
    perimeter = perimeter/480000 #so value is between 0-1
    convex = cv2.isContourConvex(cnt)
    if convex == False:
        convex = 0
    else:
        convex = 1
        
    ecen = eccentricity(img,cnt)
    compact = compactness(img,cnt)
    roundness = circularity(img, cnt)
    return area,perimeter,convex, ecen, compact, roundness,convexity(img,cnt)

def hsv(img):
    hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
    h = img.shape[0]
    w = img.shape[1]
    minH = 0
    minS = 0
    minV = 0
    maxH = 0
    maxS = 0
    maxV = 0
    
    hArray = []
    sArray = []
    vArray = []
    
    for y in range(0,h):
        for x in range(0,w):
            h,s,v = img[y,x]
            if y == 0:
                if x == 0:
                    minH = h
                    minS = s
                    minV = v
                    maxH = h
                    maxS = s
                    maxV = v
                    
            if h > maxH:
                maxH = h
            if h < minH:
                minH = h
                
            if s > maxS:
                maxS = s
            if s < minS:
                minS = s
                
            if v > maxV:
                maxV = v
            if v< minV:
                minV = v
                
            hArray.append(h)
            sArray.append(s)
            vArray.append(v)
    
    hLen = len(hArray)
    sLen = len(sArray)
    vLen = len(vArray)
    hArray = np.array(hArray)
    sArray = np.array(sArray)
    vArray = np.array(vArray)
    meanH = np.sum(hArray)/hLen
    meanS = np.sum(sArray)/sLen
    meanV = np.sum(vArray)/vLen
    
        
    #sd
    sdH = np.std(hArray)
    sdS = np.std(sArray)
    sdV = np.std(vArray)
    
    #skew
    skewH = skew(hArray)
    skewS = skew(sArray)
    skewV = skew(vArray)
    
    #ent
    entH = 0
    entS = 0
    entV = 0
    
    
    #var
    varH = np.var(hArray)
    varS = np.var(sArray)
    varV = np.var(vArray)
                
            
    
    
    

    return minH,minS,minV,maxH,maxS,maxV,meanH,meanS,meanV,sdH,sdS,sdV,skewH,skewS,skewV,entH,entS,entV,varH,varS,varV

def rgb(img):
    h = img.shape[0]
    w = img.shape[1]
    minB = 0
    minG = 0
    minR = 0
    maxB = 0
    maxG = 0
    maxR = 0
    
    rArray = []
    gArray = []
    bArray = []
    for y in range(0,h):
        for x in range(0,w):
            b,g,r = img[y,x]
            if y == 0:
                if x == 0:
                    minB = b
                    minG = g
                    minR = r
                    maxB = b
                    maxG = g
                    maxR = r
                    
            if b > maxB:
                maxB = b
            if b < minB:
                minB = b
                
            if g > maxG:
                maxG = g
            if g < minG:
                minG = g
                
            if r > maxR:
                maxR = r
            if r< minR:
                minR = r
            
            rArray.append(r)
            gArray.append(g)
            bArray.append(b)
            
    
    #mean
    rLen = len(rArray)
    gLen = len(gArray)
    bLen = len(bArray)
    rArray = np.array(rArray)
    gArray = np.array(gArray)
    bArray = np.array(bArray)
    meanR = np.sum(rArray)/rLen
    meanG = np.sum(gArray)/gLen
    meanB = np.sum(bArray)/bLen
    
        
    #sd
    sdR = np.std(rArray)
    sdG = np.std(gArray)
    sdB = np.std(bArray)
    
    #skew
    skewR = skew(rArray)
    skewB = skew(bArray)
    skewG = skew(gArray)
    
    #ent
    entR = 0
    entG = 0
    entB = 0
    
    
    #var
    varR = np.var(rArray)
    varG = np.var(gArray)
    varB = np.var(bArray)

    return minR,minG,minB,maxR,maxG, maxB, meanR,meanG, meanB,sdR,sdG,sdB, skewR,skewG,skewB, entR,entG,entB, varR,varG,varB

def lab(img):
    lab = cv2.cvtColor(img,cv2.COLOR_BGR2LAB)
    h = img.shape[0]
    w = img.shape[1]
    minL = 0
    minA = 0
    minB = 0
    maxL = 0
    maxA = 0
    maxB = 0
    
    lArray = []
    aArray = []
    bArray = []
    for y in range(0,h):
        for x in range(0,w):
            l,a,b = img[y,x]
            if y == 0:
                if x == 0:
                    minL = l
                    minA = a
                    minB = b
                    maxL = l
                    maxA = a
                    maxB = b
                    
            if l > maxL:
                maxL = l
            if l < minL:
                minL = l
                
            if a > maxA:
                maxA = a
            if a < minA:
                minA = a
                
            if b > maxB:
                maxB = b
            if b< minB:
                minB = b
            
            lArray.append(l)
            aArray.append(a)
            bArray.append(b)
            
    
    #mean
    lLen = len(lArray)
    aLen = len(aArray)
    bLen = len(bArray)
    lArray = np.array(lArray)
    aArray = np.array(aArray)
    bArray = np.array(bArray)
    meanL = np.sum(lArray)/lLen
    meanA = np.sum(aArray)/aLen
    meanB = np.sum(bArray)/bLen
    
        
    #sd
    sdL = np.std(lArray)
    sdA = np.std(aArray)
    sdB = np.std(bArray)
    
    #skew
    skewL = skew(lArray)
    skewA = skew(aArray)
    skewB = skew(bArray)
    
    #ent
    entL= 0
    entA= 0
    entB = 0
    
    
    #var
    varL = np.var(lArray)
    varA = np.var(aArray)
    varB = np.var(bArray)

    
    return minL,minA,minB,maxL,maxA,maxB,meanL,meanA,meanB,sdL,sdA,sdB,skewL,skewA,skewB,entL,entA,entB,varL,varA,varB

    
def colour(img):
    img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    glcm = skimage.feature.graycomatrix(img,[1],[0,np.pi/2],normed = True)
    energy = skimage.feature.graycoprops(glcm,'energy')[0,0]
    # = skimage.feature.graycoprops(glcm, 'dissimilarity')[0, 0]
    correlation = skimage.feature.graycoprops(glcm, 'correlation')[0, 0]
    homogeny = skimage.feature.graycoprops(glcm, 'homogeneity')[0, 0]
    
    return homogeny,energy, correlation #, dissimilarity


flagE = 0
def colourLoop():
    minL = []
    minA= []
    minB= []
    maxL= []
    maxA= []
    maxB= []
    meanL= []
    meanA= []
    meanB= []
    sdL= []
    sdA= []
    sdB= []
    skewL= []
    skewA= []
    skewB= []
    entL= []
    entA= []
    entB= []
    varL= []
    varA= []
    varB = []
    
    
    minR = []
    minG= []
    minBl= []
    maxR= []
    maxG= []
    maxBl= []
    meanR= []
    meanG= []
    meanBl= []
    sdR= []
    sdG= []
    sdBl= []
    skewR= []
    skewG= []
    skewBl= []
    entR= []
    entG= []
    entBl= []
    varR= []
    varG= []
    varBl = []
    
    minH = []
    minS= []
    minV= []
    maxH= []
    maxS= []
    maxV= []
    meanH= []
    meanS= []
    meanV= []
    sdH= []
    sdS= []
    sdV= []
    skewH= []
    skewS= []
    skewV= []
    entH= []
    entS= []
    entV= []
    varH= []
    varS= []
    varV = []
    
    count = 0
    labels = prepDataset()
    for label in labels:
        print(count/len(labels)*100)
        print("%")
        string = "ISIC-2017_Test_v2_Data/" + label.imageName + ".jpg"
        if 1==1:
            img = cv2.imread(string,cv2.IMREAD_COLOR )
            img = cv2.resize(img,(600,450))
            print(count)
            count = count+1
            array = hsv(img)
            minL.append(array[0])
            minA.append(array[1])
            minB.append(array[2])
            maxL.append(array[3])
            maxA.append(array[4])
            maxB.append(array[5])
            meanL.append(array[6])
            meanA.append(array[7])
            meanB.append(array[8])
            sdL.append(array[9])
            sdA.append(array[10])
            sdB.append(array[11])
            skewL.append(array[12])
            skewA.append(array[13])
            skewB.append(array[14])
            entL.append(array[15])
            entA.append(array[16])
            entB.append(array[17])
            varL.append(array[18])
            varA.append(array[19])
            varB.append(array[20])

            
            array = rgb(img)
            minR.append(array[0])
            minG.append(array[1])
            minBl.append(array[2])
            maxR.append(array[3])
            maxG.append(array[4])
            maxBl.append(array[5])
            meanR.append(array[6])
            meanG.append(array[7])
            meanBl.append(array[8])
            sdR.append(array[9])
            sdG.append(array[10])
            sdBl.append(array[11])
            skewR.append(array[12])
            skewG.append(array[13])
            skewBl.append(array[14])
            entR.append(array[15])
            entG.append(array[16])
            entBl.append(array[17])
            varR.append(array[18])
            varG.append(array[19])
            varBl.append(array[20])
            
            array = rgb(img)
            minH.append(array[0])
            minS.append(array[1])
            minV.append(array[2])
            maxH.append(array[3])
            maxS.append(array[4])
            maxV.append(array[5])
            meanH.append(array[6])
            meanS.append(array[7])
            meanV.append(array[8])
            sdH.append(array[9])
            sdS.append(array[10])
            sdV.append(array[11])
            skewH.append(array[12])
            skewS.append(array[13])
            skewV.append(array[14])
            entH.append(array[15])
            entS.append(array[16])
            entV.append(array[17])
            varH.append(array[18])
            varS.append(array[19])
            varV.append(array[20])
        
    #lab
    minL = np.array(minL)
    minA =  np.array(minA)
    minB = np.array(minB)
    
    maxL= np.array(maxL)
    maxA= np.array(maxA)
    maxB = np.array(maxB)
    
    meanL = np.array(meanL)
    meanA = np.array(meanA)
    meanB = np.array(meanB)
    
    sdL = np.array(sdL)
    sdA = np.array(sdA)
    sdB = np.array(sdB)
    
    skewL = np.array(skewL)
    skewA = np.array(skewA)
    skewB = np.array(skewB)
    
    entL = np.array(entL)
    entA = np.array(entA)
    entB = np.array(entB)
    
    varL = np.array(varL)
    varA = np.array(varA)
    varB = np.array(varB)
    
    
    
    
    
    
    #rgb
    minR = np.array(minR)
    minG = np.array(minG)
    minBl = np.array(minBl)
    
    maxR = np.array(maxR)
    maxG = np.array(maxG)
    maxBl = np.array(maxBl)
    
    meanR = np.array(meanR)
    meanG = np.array(meanG)
    meanBl = np.array(meanBl)
    
    sdR = np.array(sdR)
    sdG = np.array(sdG)
    sdBl = np.array(sdBl)
    
    skewR = np.array(skewR)
    skewG = np.array(skewG)
    skewBl = np.array(skewBl)
    
    entR = np.array(entR)
    entG = np.array(entG)
    entBl = np.array(entBl)
    
    varR = np.array(varR)
    varG = np.array(varG)
    varBl = np.array(varBl)
    
    
    
    #hsv
    minH  = np.array(minH)
    minS = np.array(minS)
    minV = np.array(minV)
    maxH = np.array(maxH)
    maxS = np.array(maxS)
    maxV = np.array(maxV)
    meanH = np.array(meanH)
    meanS = np.array(meanS)
    meanV = np.array(meanV)
    sdH = np.array(sdH)
    sdS = np.array(sdS)
    sdV = np.array(sdV)
    skewH = np.array(skewH)
    skewS = np.array(skewS)
    skewV = np.array(skewV)
    entH = np.array(entH)
    entS = np.array(entS)
    entV = np.array(entV)
    varH = np.array(varH)
    varS = np.array(varS)
    varV = np.array(varV)
    
    np.save('preppedData/traditionalTest/minL',minL)
    np.save('preppedData/traditionalTest/minA',minA)
    np.save('preppedData/traditionalTest/minB',minB)
    
    np.save('preppedData/traditionalTest/maxL',maxL)
    np.save('preppedData/traditionalTest/maxA',maxA)
    np.save('preppedData/traditionalTest/maxB',maxB)
    
    np.save('preppedData/traditionalTest/meanL',meanL)
    np.save('preppedData/traditionalTest/meanA',meanA)
    np.save('preppedData/traditionalTest/meanB',meanB)
    
    np.save('preppedData/traditionalTest/sdL',sdL)
    np.save('preppedData/traditionalTest/sdA',sdA)
    np.save('preppedData/traditionalTest/sdB',sdB)
    
    np.save('preppedData/traditionalTest/skewL',skewL)
    np.save('preppedData/traditionalTest/skewA',skewA)
    np.save('preppedData/traditionalTest/skewB',skewB)
    
    np.save('preppedData/traditionalTest/entL',entL)
    np.save('preppedData/traditionalTest/entA',entA)
    np.save('preppedData/traditionalTest/entB',entB)
    
    np.save('preppedData/traditionalTest/varL',varL)
    np.save('preppedData/traditionalTest/varA',varA)
    np.save('preppedData/traditionalTest/varB',varB)
    
    
    
    
    
    np.save('preppedData/traditionalTest/minR',minR)
    np.save('preppedData/traditionalTest/minG',minG)
    np.save('preppedData/traditionalTest/minBl',minBl)
    
    np.save('preppedData/traditionalTest/maxR',maxR)
    np.save('preppedData/traditionalTest/maxG',maxG)
    np.save('preppedData/traditionalTest/maxBl',maxBl)
    
    np.save('preppedData/traditionalTest/meanR',meanR)
    np.save('preppedData/traditionalTest/meanG',meanG)
    np.save('preppedData/traditionalTest/meanBl',meanBl)
    
    np.save('preppedData/traditionalTest/sdR',sdR)
    np.save('preppedData/traditionalTest/sdG',sdG)
    np.save('preppedData/traditionalTest/sdBl',sdBl)
    
    np.save('preppedData/traditionalTest/skewR',skewR)
    np.save('preppedData/traditionalTest/skewG',skewG)
    np.save('preppedData/traditionalTest/skewBl',skewBl)
    
    np.save('preppedData/traditionalTest/entR',entR)
    np.save('preppedData/traditionalTest/entG',entG)
    np.save('preppedData/traditionalTest/entBl',entBl)
    
    np.save('preppedData/traditionalTest/varR',varR)
    np.save('preppedData/traditionalTest/varG',varG)
    np.save('preppedData/traditionalTest/varBl',varBl)
    
    
    
    
    
    np.save('preppedData/traditionalTest/minH',minH)
    np.save('preppedData/traditionalTest/minS',minS)
    np.save('preppedData/traditionalTest/minV',minV)
    
    np.save('preppedData/traditionalTest/maxH',maxH)
    np.save('preppedData/traditionalTest/maxS',maxS)
    np.save('preppedData/traditionalTest/maxV',maxV)
    
    np.save('preppedData/traditionalTest/meanH',meanH)
    np.save('preppedData/traditionalTest/meanS',meanS)
    np.save('preppedData/traditionalTest/meanV',meanV)
    
    np.save('preppedData/traditionalTest/sdH',sdH)
    np.save('preppedData/traditionalTest/sdS',sdS)
    np.save('preppedData/traditionalTest/sdV',sdV)
    
    np.save('preppedData/traditionalTest/skewH',skewH)
    np.save('preppedData/traditionalTest/skewS',skewS)
    np.save('preppedData/traditionalTest/skewV',skewV)
    
    np.save('preppedData/traditionalTest/entH',entH)
    np.save('preppedData/traditionalTest/entS',entS)
    np.save('preppedData/traditionalTest/entV',entV)
    
    np.save('preppedData/traditionalTest/varH',varH)
    np.save('preppedData/traditionalTest/varS',varS)
    np.save('preppedData/traditionalTest/varV',varV)


    
    
    
if(os.path.exists("preppedData/traditionalTest/symmetryList.npy")):
    flagE = 1

if(os.path.exists("preppedData/traditionalTest/entH.npy")):
    print("values pt1 loaded")
else:
    colourLoop()

if flagE == 0:
    symmetryList,homogenyList,energyList, correlationList,areaList,perimeterList, convexList, ecenList, compactList,roundnessList,convexValList,ageList, sexList,labelList = loopDirectory("training")
    symmetryList = np.array(symmetryList)
    homogenyList = np.array(homogenyList)
    energyList = np.array(energyList)
    correlationList = np.array(correlationList)
    areaList = np.array(areaList)
    perimeterList = np.array(perimeterList)
    convexList = np.array(convexList)
    ecenList = np.array(ecenList)
    compactList = np.array(compactList)
    roundnessList = np.array(roundnessList)
    convexValList = np.array(convexValList)
    labelList= np.array(labelList)
    
    
    np.save('preppedData/traditionalTest/symmetryList',symmetryList)
    np.save('preppedData/traditionalTest/homogenyList', homogenyList)
    np.save('preppedData/traditionalTest/energyList', energyList)
    np.save('preppedData/traditionalTest/correlationList', correlationList)
    np.save('preppedData/traditionalTest/areaList', areaList)
    np.save('preppedData/traditionalTest/perimeterList', perimeterList)
    np.save('preppedData/traditionalTest/convexList', convexList)
    np.save('preppedData/traditionalTest/ecenList', ecenList)
    np.save('preppedData/traditionalTest/compactList', compactList)
    np.save('preppedData/traditionalTest/roundnessList', roundnessList)
    np.save('preppedData/traditionalTest/convexValList', convexValList)
    np.save('preppedData/traditionalTest/labelList', labelList)
else:
    print("values pt2 loaded")
    symmetryList = np.load('preppedData/traditionalTest/symmetryList.npy')
    homogenyList = np.load('preppedData/traditionalTest/homogenyList.npy')
    energyList = np.load('preppedData/traditionalTest/energyList.npy')
    correlationList = np.load('preppedData/traditionalTest/correlationList.npy')
    areaList = np.load('preppedData/traditionalTest/areaList.npy')
    perimeterList = np.load('preppedData/traditionalTest/perimeterList.npy')
    convexList = np.load('preppedData/traditionalTest/convexList.npy')
    ecenList = np.load('preppedData/traditionalTest/ecenList.npy')
    compactList = np.load('preppedData/traditionalTest/compactList.npy')
    roundnessList = np.load('preppedData/traditionalTest/roundnessList.npy')
    convexValList = np.load('preppedData/traditionalTest/convexValList.npy')
    labelList= np.load('preppedData/traditionalTest/labelList.npy')

#loopDirectory("images1")
