import math

class DataEntry:
    def __init__(self,name,TN,FP,FN,TP):
        self.name = name
        self.TN = TN
        self.FP = FP
        self.FN = FN
        self.TP = TP


def MCC(entry):
    mccVal = (entry.TP*entry.TN) - (entry.FP * entry.FN)
    inSqrt = (entry.TP+entry.FP)*(entry.TP+entry.FN)*(entry.TN+entry.FP)*(entry.TN+entry.FN)
    if inSqrt == 0:
        return 0
    mccVal = mccVal/(math.sqrt(inSqrt))
    return mccVal

def fpRate(entry):
    if entry.TN+entry.FP == 0:
        return "n/a"
    fp = (entry.FP)/(entry.TN+entry.FP)
    return fp

def precision(entry):
    if entry.TP+entry.FP == 0:
        return "n/a"
    
    
    prec= entry.TP/(entry.TP+entry.FP)
    print(entry.FP)
    return prec
    
def recall(entry):
    rec = entry.TP/(entry.TP+entry.FN)
    return rec
    
def fMeasure(prec,rec):
    fM = 2*(prec*rec)/(prec+rec)
    return fM
    
    


def prepMatrix():
    file = open("ratesForWork","r")
    fileWrite = open("rateOutput.txt","w")
    fileRead = file.read().splitlines()
    for lines in fileRead:
        words = lines.split(",")
        entry = DataEntry(words[0],float(words[1]),float(words[2]),float(words[3]),float(words[4]))
        mcc = MCC(entry)
        fp = fpRate(entry)
        prec = precision(entry)
        rec = recall(entry)
        fM = fMeasure(prec,rec)
        name = words[0]
        string = "\n name: " + name
        string = string + "\n FP RATE: " + str(fp)
        string = string + "\n Precsion: " + str(prec)
        string = string + "\n recall: " + str(rec)
        string = string + "\n F-Measure: " + str(fM)
        string = string + "\n MCC: " + str(mcc) + "\n"
        print(string)
        fileWrite.write(string)

prepMatrix()