import math

class DataEntry:
    def __init__(self,name,TN,FP,FN,TP):
        self.name = name
        self.TN = TN
        self.FP = FP
        self.FN = FN
        self.TP = TP


def TumourAcc(entry):
    return entry.TP/(entry.FN+entry.TP)*100

def NonTumourAcc(entry):
    return entry.TN/(entry.FP+entry.TN)*100

def Acc(entry):
    return (entry.TN+entry.TP)/ (entry.TN+entry.TP+entry.FP+entry.FN)*100
    

    
    


def prepMatrix():
    file = open("ratesForWork","r")
    fileWrite = open("percentageOutput.txt","w")
    fileRead = file.read().splitlines()
    for lines in fileRead:
        words = lines.split(",")
        entry = DataEntry(words[0],float(words[1]),float(words[2]),float(words[3]),float(words[4]))
        tumourAcc = TumourAcc(entry)
        nonTumourAcc = NonTumourAcc(entry)
        acc = Acc(entry)
        name = words[0]
        string = "\n \n name: " + name
        string = string + "\n Tumour accuracy: " + str(tumourAcc)
        string = string + "\n Non-tumour accuracy: " + str(nonTumourAcc)
        string = string + "\n Total Accuracy: " + str(acc)
        print(string)
        fileWrite.write(string)

prepMatrix()