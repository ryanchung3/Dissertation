import cv2
import numpy as np
import os
import sys
import math
from PIL import Image  
import PIL  
from matplotlib import pyplot
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from sklearn.utils import shuffle
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from numpy import savetxt,loadtxt
from tensorflow.keras.applications.resnet50 import preprocess_input, decode_predictions


print(len(tf.config.experimental.list_physical_devices('GPU')))
#augment data


def labelImages():
    return
def resizeImg(img):
    return img

def showImage(imgNum):
    #showsImage, imgNum = imgNum
    img = cv2.imread("images1/ISIC_"+str(imgNum)+".jpg",cv2.IMREAD_COLOR ) #loads image
    test = hairRemoval(img)
    gray = cv2.cvtColor(test,cv2.COLOR_BGR2GRAY)
    #cv2.imshow("test Image", test)
    median = cv2.medianBlur(gray, 9)
    ret, th = cv2.threshold(median,0,255,cv2.THRESH_OTSU) #thresholding


    #tests
    kernel = np.ones((14,14),np.uint8) #kernel
    #erosion = cv2.erode(th,kernel,iterations=3) #erodes image
    #dilation = cv2.dilate(erosion,kernel, iterations=3) #dilation
    #opening = cv2.morphologyEx(th,cv2.MORPH_OPEN,kernel) #erosion + dilation
    #morph = cv2.morphologyEx(th, cv2.MORPH_GRADIENT, kernel)
    
    
    
    #close
    close = cv2.morphologyEx(th,cv2.MORPH_CLOSE,kernel)
    
    cv2.imshow("Original Image", img)
    #cv2.imshow("Median Image",median)
    #cv2.imshow("OTSU Image",th) #median threshold
    #cv2.imshow("Erode/Dilated Image", dilation)
    #cv2.imshow("mroph", morph)
    #cv2.imshow("close",close)
    #cv2.imshow("Open Image", opening)
    
    
    #finding contours
    closeInv = cv2.bitwise_not(close) #inverts image
    result = np.zeros_like(closeInv)
    overlayedMask = overlayMask(img, result,closeInv) #overlays mask over original image
    result = expandImg(result) #expands image
    closeInv = expandImg(closeInv) #expands image
    contours,hierachy = cv2.findContours(closeInv, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    #draws largest contour
    bContour = max(contours, key = cv2.contourArea)
    cv2.drawContours(result,[bContour],-1,(255,255,255),-1)
    
    
    #removing noise
    result = cv2.medianBlur(result, 9)
    result = cv2.medianBlur(result, 9)
    
    
        
    
    symmetry = sym(result,bContour) # returns symmetry as a value between 0 and 1. The higher the value, the more symmetric
    print(symmetry)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def overlayMask(img,result,closeInv):


    # finding contours
    contours, hierachy = cv2.findContours(closeInv, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # draws largest contour
    bContour = max(contours, key=cv2.contourArea)
    cv2.drawContours(result, [bContour], -1, (255, 255, 255), -1)
    # removing noise
    result = cv2.medianBlur(result, 9)
    result = cv2.medianBlur(result, 9)

    #cv2.imshow("overlay mask",result) #shows non rotated mask

    mask = cv2.bitwise_and(img, img, mask=result)
    return mask

    #cv2.imshow("overlay mask2 ",mask)
    


def loopDirectory(dir):
    directory = os.fsencode(dir)
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".jpg"):
            #sys.stdout.write(filename)
            continue
        else:
            continue
def expandImg(img):
    top = bottom = left = right = 1280
    dst = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, (0, 0, 0))
    #cv2.imshow("boxExpanded", dst)
    return dst


def sym(img,cnt):
    #analyses symmetry
    flip = 0
    rect = cv2.minAreaRect(cnt)
    center = rect[0]
    xyLen = rect[1]
    angle = rect[2]
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    rgb = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
    cv2.drawContours(rgb,[box],0,(0,0,0),2)
    cropped = cv2.getRectSubPix(rgb,(int(xyLen[0]),int(xyLen[1])),center)
    flipx = 0
    flipy = 0
    if angle<-45:
        angle= angle+90
        flipx = xyLen[1]
        flipy = xyLen[0]
    else:
        flipx =  xyLen[0]
        flipy = xyLen[1]


    flipM = cv2.getRotationMatrix2D(center, angle, 1)
    (h, w) = rgb.shape[:2]
    rotated = cv2.warpAffine(rgb, flipM, (w, h)) #rotates to straight
    croppedRot = cv2.getRectSubPix(rotated,(int(flipx),int(flipy)),center) #crops image

    #cv2.imshow("cropped", croppedRot) #dusplays cropped image

    croppedRot = cv2.cvtColor(croppedRot, cv2.COLOR_BGR2GRAY)

    #create clone
    croppedClone = croppedRot
    (h, w) = croppedClone.shape[:2]
    if w>h:
        flip = 0
    else:
        flip = 1
    flippedClone = cv2.flip(croppedClone,flip)
    a = cv2.bitwise_or(croppedRot,flippedClone) #Lesion union Symmetry mask
    fs = cv2.bitwise_xor(croppedRot,a) #false symmetry
    cv2.imshow("asym", fs)
    #sym = 1 - (fs/a)
    #Formula taken from "AUTOMATIC BOUNDARY DETECTION AND SYMMETRY CALCULATION IN DEMOSCOPY IMAGES OF SKIN LESIONS"
    aCount = cv2.countNonZero(a)#counts white pixels
    fsCount = cv2.countNonZero(fs)#counts white pixels
    symmetry = 1-(fsCount/aCount)

    return symmetry



    


def hairRemoval(img):

    
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    #cv2.imshow("edges",gray)
    kernel = np.ones((10,10),np.uint8)
    blackhat = cv2.morphologyEx(gray,cv2.MORPH_BLACKHAT, kernel)
    #cv2.imshow("edges2",blackhat)
    ret, th = cv2.threshold(blackhat,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    #cv2.imshow("thresh",th)
    output = cv2.inpaint(img,th,1,cv2.INPAINT_TELEA)
    #cv2.imshow("lines thing",output)
    
    #output = hairRemoval(output)
    
    return output
    
    

def shape(img):
    #analyses shape
    print("hi")



while(1):
    new_model = tf.keras.models.load_model('BestModel/0.01cnnAugEp128')
    print("Enter the file path for the image you want to test.")
    imagePath = input()
    if(os.path.exists(imagePath)):
        t = cv2.imread(imagePath,cv2.IMREAD_COLOR )
        t = cv2.resize(t,(75,100))
        prediction = new_model.predict(t.reshape(-1,75,100,3))
        array = []
        cnt = 0
        pred = 1-  prediction[0][1]
        if 1-pred >0.5:
            print("Tumour detected")
        else:
            print("No tumour detected.")
    else:
        print("file not found try again")
    
#loss, acc = new_model.evaluate(trIterator, steps=len(trIterator), verbose=1)


