import cv2
import numpy as np
import os
import sys
import math
from PIL import Image  
import PIL  
from matplotlib import pyplot
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
import skimage
from scipy.stats import skew, entropy


def createArff():
    flagE = 0
    if(os.path.exists("preppedData/traditionalTest/symmetryList.npy")):
        flagE = 1
    
    if flagE == 0:
        print("error: no values found")
    else:
        symmetryList = np.load('preppedData/traditionalTest/symmetryList.npy')
        homogenyList = np.load('preppedData/traditionalTest/homogenyList.npy')
        energyList = np.load('preppedData/traditionalTest/energyList.npy')
        correlationList = np.load('preppedData/traditionalTest/correlationList.npy')
        areaList = np.load('preppedData/traditionalTest/areaList.npy')
        perimeterList = np.load('preppedData/traditionalTest/perimeterList.npy')
        convexList = np.load('preppedData/traditionalTest/convexList.npy')
        ecenList = np.load('preppedData/traditionalTest/ecenList.npy')
        compactList = np.load('preppedData/traditionalTest/compactList.npy')
        roundnessList = np.load('preppedData/traditionalTest/roundnessList.npy')
        convexValList = np.load('preppedData/traditionalTest/convexValList.npy')
        labelList= np.load('preppedData/traditionalTest/labelList.npy')
        
        
        
        
        
        
        
        
    minL = np.load('preppedData/traditionalTest/minL.npy')
    minA= np.load('preppedData/traditionalTest/minA.npy')
    minB= np.load('preppedData/traditionalTest/minB.npy')
    
    maxL= np.load('preppedData/traditionalTest/maxL.npy')
    maxA= np.load('preppedData/traditionalTest/maxA.npy')
    maxB= np.load('preppedData/traditionalTest/maxB.npy')
    
    meanL= np.load('preppedData/traditionalTest/meanL.npy')
    meanA= np.load('preppedData/traditionalTest/meanA.npy')
    meanB= np.load('preppedData/traditionalTest/meanB.npy')
    
    sdL= np.load('preppedData/traditionalTest/sdL.npy')
    sdA= np.load('preppedData/traditionalTest/sdA.npy')
    sdB= np.load('preppedData/traditionalTest/sdB.npy')
    
    skewL= np.load('preppedData/traditionalTest/skewL.npy')
    skewA= np.load('preppedData/traditionalTest/skewA.npy')
    skewB= np.load('preppedData/traditionalTest/skewB.npy')
    
    entL= np.load('preppedData/traditionalTest/entL.npy')
    entA= np.load('preppedData/traditionalTest/entA.npy')
    entB= np.load('preppedData/traditionalTest/entB.npy')
    
    varL= np.load('preppedData/traditionalTest/varL.npy')
    varA= np.load('preppedData/traditionalTest/varA.npy')
    varB= np.load('preppedData/traditionalTest/varB.npy')
    
    
    
    
    
    minR = np.load('preppedData/traditionalTest/minR.npy')
    minG= np.load('preppedData/traditionalTest/minG.npy')
    minBl= np.load('preppedData/traditionalTest/minBl.npy')
    
    maxR = np.load('preppedData/traditionalTest/maxR.npy')
    maxG= np.load('preppedData/traditionalTest/maxG.npy',)
    maxBl= np.load('preppedData/traditionalTest/maxBl.npy')
    
    meanR= np.load('preppedData/traditionalTest/meanR.npy')
    meanG= np.load('preppedData/traditionalTest/meanG.npy')
    meanBl= np.load('preppedData/traditionalTest/meanBl.npy')
    
    sdR= np.load('preppedData/traditionalTest/sdR.npy')
    sdG= np.load('preppedData/traditionalTest/sdG.npy')
    sdBl= np.load('preppedData/traditionalTest/sdBl.npy')
    
    skewR= np.load('preppedData/traditionalTest/skewR.npy')
    skewG= np.load('preppedData/traditionalTest/skewG.npy')
    skewBl= np.load('preppedData/traditionalTest/skewBl.npy')
    
    entR= np.load('preppedData/traditionalTest/entR.npy')
    entG =np.load('preppedData/traditionalTest/entG.npy')
    entBl= np.load('preppedData/traditionalTest/entBl.npy')
    
    varR= np.load('preppedData/traditionalTest/varR.npy')
    varG= np.load('preppedData/traditionalTest/varG.npy')
    varBl= np.load('preppedData/traditionalTest/varBl.npy')
    
    
    
    minH= np.load('preppedData/traditionalTest/minH.npy')
    minS= np.load('preppedData/traditionalTest/minS.npy')
    minV= np.load('preppedData/traditionalTest/minV.npy')
    
    maxH= np.load('preppedData/traditionalTest/maxH.npy')
    maxS= np.load('preppedData/traditionalTest/maxS.npy')
    maxV= np.load('preppedData/traditionalTest/maxV.npy')
    
    meanH= np.load('preppedData/traditionalTest/meanH.npy')
    meanS= np.load('preppedData/traditionalTest/meanS.npy')
    meanV= np.load('preppedData/traditionalTest/meanV.npy')
    
    sdH= np.load('preppedData/traditionalTest/sdH.npy')
    sdS= np.load('preppedData/traditionalTest/sdS.npy')
    sdV = np.load('preppedData/traditionalTest/sdV.npy')
    
    skewH= np.load('preppedData/traditionalTest/skewH.npy')
    skewS= np.load('preppedData/traditionalTest/skewS.npy')
    skewV= np.load('preppedData/traditionalTest/skewV.npy')
    
    entH= np.load('preppedData/traditionalTest/entH.npy')
    entS= np.load('preppedData/traditionalTest/entS.npy')
    entV= np.load('preppedData/traditionalTest/entV.npy')
    
    varH= np.load('preppedData/traditionalTest/varH.npy')
    varS= np.load('preppedData/traditionalTest/varS.npy')
    varV= np.load('preppedData/traditionalTest/varV.npy')
        
        
    file = open("lesionTest.arff","w")
    file.write("@RELATION skinLesions\n")
    file.write("@ATTRIBUTE symmetry          NUMERIC\n")
    file.write("@ATTRIBUTE homogeny          NUMERIC\n")
    file.write("@ATTRIBUTE energy            NUMERIC\n")
    file.write("@ATTRIBUTE correlation       NUMERIC\n")
    file.write("@ATTRIBUTE normArea          NUMERIC\n")
    file.write("@ATTRIBUTE normPerimeter     NUMERIC\n")
    file.write("@ATTRIBUTE eccentricity      NUMERIC\n")
    file.write("@ATTRIBUTE compactness       NUMERIC\n")
    file.write("@ATTRIBUTE roundness         NUMERIC\n")
    file.write("@ATTRIBUTE convexity         NUMERIC\n")

    
    file.write("@ATTRIBUTE minL              NUMERIC\n")
    file.write("@ATTRIBUTE minA              NUMERIC\n")
    file.write("@ATTRIBUTE minB              NUMERIC\n")
    file.write("@ATTRIBUTE maxL              NUMERIC\n")
    file.write("@ATTRIBUTE maxA              NUMERIC\n")
    file.write("@ATTRIBUTE maxB              NUMERIC\n")
    file.write("@ATTRIBUTE meanL             NUMERIC\n")
    file.write("@ATTRIBUTE meanA             NUMERIC\n")
    file.write("@ATTRIBUTE meanB             NUMERIC\n")
    file.write("@ATTRIBUTE sdL               NUMERIC\n")
    file.write("@ATTRIBUTE sdA               NUMERIC\n")
    file.write("@ATTRIBUTE sdB               NUMERIC\n")
    file.write("@ATTRIBUTE skewL             NUMERIC\n")
    file.write("@ATTRIBUTE skewA             NUMERIC\n")
    file.write("@ATTRIBUTE skewB             NUMERIC\n")
    file.write("@ATTRIBUTE varL              NUMERIC\n")
    file.write("@ATTRIBUTE varA              NUMERIC\n")
    file.write("@ATTRIBUTE varB              NUMERIC\n")
    
    
    file.write("@ATTRIBUTE minR              NUMERIC\n")
    file.write("@ATTRIBUTE minG              NUMERIC\n")
    file.write("@ATTRIBUTE minBl             NUMERIC\n")
    file.write("@ATTRIBUTE maxR              NUMERIC\n")
    file.write("@ATTRIBUTE maxG              NUMERIC\n")
    file.write("@ATTRIBUTE maxBl             NUMERIC\n")
    file.write("@ATTRIBUTE meanR             NUMERIC\n")
    file.write("@ATTRIBUTE meanG             NUMERIC\n")
    file.write("@ATTRIBUTE meanBl            NUMERIC\n")
    file.write("@ATTRIBUTE sdR               NUMERIC\n")
    file.write("@ATTRIBUTE sdG               NUMERIC\n")
    file.write("@ATTRIBUTE sdBl              NUMERIC\n")
    file.write("@ATTRIBUTE skewR             NUMERIC\n")
    file.write("@ATTRIBUTE skewG             NUMERIC\n")
    file.write("@ATTRIBUTE skewBl            NUMERIC\n")
    file.write("@ATTRIBUTE varR              NUMERIC\n")
    file.write("@ATTRIBUTE varG              NUMERIC\n")
    file.write("@ATTRIBUTE varBl             NUMERIC\n")
    
    
    file.write("@ATTRIBUTE minH              NUMERIC\n")
    file.write("@ATTRIBUTE minS              NUMERIC\n")
    file.write("@ATTRIBUTE minV              NUMERIC\n")
    file.write("@ATTRIBUTE maxH              NUMERIC\n")
    file.write("@ATTRIBUTE maxS              NUMERIC\n")
    file.write("@ATTRIBUTE maxV              NUMERIC\n")
    file.write("@ATTRIBUTE meanH             NUMERIC\n")
    file.write("@ATTRIBUTE meanS             NUMERIC\n")
    file.write("@ATTRIBUTE meanV             NUMERIC\n")
    file.write("@ATTRIBUTE sdH               NUMERIC\n")
    file.write("@ATTRIBUTE sdS               NUMERIC\n")
    file.write("@ATTRIBUTE sdV               NUMERIC\n")
    file.write("@ATTRIBUTE skewH             NUMERIC\n")
    file.write("@ATTRIBUTE skewS             NUMERIC\n")
    file.write("@ATTRIBUTE skewV             NUMERIC\n")
    file.write("@ATTRIBUTE varH              NUMERIC\n")
    file.write("@ATTRIBUTE varS              NUMERIC\n")
    file.write("@ATTRIBUTE varV              NUMERIC\n")

    
    
    file.write("@ATTRIBUTE CLASS             {1.0,0.0}\n")
    file.write("@data\n")
    
    for i in range(len(labelList)):
        file.write(str(symmetryList[i]))
        file.write(",")
        file.write(str(homogenyList[i]))
        file.write(",")
        file.write(str(energyList[i]))
        file.write(",")
        file.write(str(correlationList[i]))
        file.write(",")
        file.write(str(areaList[i]))
        file.write(",")
        file.write(str(perimeterList[i]))
        file.write(",")
        
            
        file.write(str(ecenList[i]))
        file.write(",")
        file.write(str(compactList[i]))
        file.write(",")
        file.write(str(roundnessList[i]))
        file.write(",")
        file.write(str(convexValList[i]))
    
            
        
        
        file.write(",")
        file.write(str(minL[i]))
        file.write(",")
        file.write(str(minA[i]))
        file.write(",")
        file.write(str(minB[i]))
        file.write(",")
        file.write(str(maxL[i]))
        file.write(",")
        file.write(str(maxA[i]))
        file.write(",")
        file.write(str(maxB[i]))
        file.write(",")
        file.write(str(meanL[i]))
        file.write(",")
        file.write(str(meanA[i]))
        file.write(",")
        file.write(str(meanB[i]))
        file.write(",")
        file.write(str(sdL[i]))
        file.write(",")
        file.write(str(sdA[i]))
        file.write(",")
        file.write(str(sdB[i]))
        file.write(",")
        file.write(str(skewL[i]))
        file.write(",")
        file.write(str(skewA[i]))
        file.write(",")
        file.write(str(skewB[i]))
        file.write(",")
        file.write(str(varL[i]))
        file.write(",")
        file.write(str(varA[i]))
        file.write(",")
        file.write(str(varB[i]))
        file.write(",")
        
        
        file.write(str(minR[i]))
        file.write(",")
        file.write(str(minG[i]))
        file.write(",")
        file.write(str(minBl[i]))
        file.write(",")
        file.write(str(maxR[i]))
        file.write(",")
        file.write(str(maxG[i]))
        file.write(",")
        file.write(str(maxBl[i]))
        file.write(",")
        file.write(str(meanR[i]))
        file.write(",")
        file.write(str(meanG[i]))
        file.write(",")
        file.write(str(meanBl[i]))
        file.write(",")
        file.write(str(sdR[i]))
        file.write(",")
        file.write(str(sdG[i]))
        file.write(",")
        file.write(str(sdBl[i]))
        file.write(",")
        file.write(str(skewR[i]))
        file.write(",")
        file.write(str(skewG[i]))
        file.write(",")
        file.write(str(skewBl[i]))
        file.write(",")
        file.write(str(varR[i]))
        file.write(",")
        file.write(str(varG[i]))
        file.write(",")
        file.write(str(varBl[i]))
        file.write(",")
        
        file.write(str(minH[i]))
        file.write(",")
        file.write(str(minS[i]))
        file.write(",")
        file.write(str(minV[i]))
        file.write(",")
        file.write(str(maxH[i]))
        file.write(",")
        file.write(str(maxS[i]))
        file.write(",")
        file.write(str(maxV[i]))
        file.write(",")
        file.write(str(meanH[i]))
        file.write(",")
        file.write(str(meanS[i]))
        file.write(",")
        file.write(str(meanV[i]))
        file.write(",")
        file.write(str(sdH[i]))
        file.write(",")
        file.write(str(sdS[i]))
        file.write(",")
        file.write(str(sdV[i]))
        file.write(",")
        file.write(str(skewH[i]))
        file.write(",")
        file.write(str(skewS[i]))
        file.write(",")
        file.write(str(skewV[i]))
        file.write(",")
        file.write(str(varH[i]))
        file.write(",")
        file.write(str(varS[i]))
        file.write(",")
        file.write(str(varV[i]))
        file.write(",")
        
        if labelList[i] == 0:
            file.write("0.0\n")
        elif labelList[i] == 1:
            file.write("1.0\n")
        else:
            print("error")


createArff()
