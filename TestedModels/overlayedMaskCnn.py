import cv2
import numpy as np
import os
import sys
import math
from PIL import Image  
import PIL  
from matplotlib import pyplot
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from sklearn.utils import shuffle
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint
from numpy import savetxt,loadtxt



print(len(tf.config.experimental.list_physical_devices('GPU')))
#augment data

class DataEntry:
    def __init__(self,lesionId,imageId,dx,dxType,age,sex,localization,dataset):
        self.lesionId = lesionId
        self.imageId = imageId
        self.dx = dx
        self.dxType = dxType
        self.age = age
        self.sex = sex
        self.localization = localization
        self.dataset = dataset
        

class ImageLabel:
    def __init__(self,imageName,image):
        self.imageName = imageName
        self.image = image
        
def prepDataset():
    file = open("HAM10000_metadata","r")
    fileRead = file.read().splitlines()
    data = []
    count = 0
    for lines in fileRead:
        words = lines.split(",")
        entry = DataEntry(words[0],words[1],words[2],words[3],words[4],words[5],words[6],words[7])
        if count!=0:
            data.append(entry)
        count = count + 1
    print(len(data))
    
    return data

def prepTrainImages():
    imageArray = []
    nameArray = []
    imageLabelA = []
    trainImages = []
    trainLabels = []
    
    count = 0
    for img in os.listdir("training"):
        print(count/10015*100)
        print("%")
        count = count + 1
        string = "training/" + img
        image = cv2.imread(string,cv2.IMREAD_COLOR)
        image = showImage(image)
        newImage = cv2.resize(image,(75,100))
        imageArray.append(newImage)
        imgName = img.replace(".jpg","")
        nameArray.append(imgName)
    
    
    count = 0
    for i in range(len(imageArray)):
        labelled = ImageLabel(nameArray[i],imageArray[i])
        imageLabelA.append(labelled)
        #print("part 2: "+ str(count))
        count = count + 1
    
    
    labels = prepDataset()
    
    
    tumourCount = 0
    nTumourCount = 0
    for image in imageLabelA:
        for label in labels:
            if label.imageId == image.imageName:
                trainImages.append(image.image)
                if label.dx == "mel" or label.dx == "bkl" or label.dx == "nv" or label.dx == "vasc":
                    trainLabels.append(1)
                    tumourCount = tumourCount +1
                else:
                    trainLabels.append(0)
                    nTumourCount = nTumourCount +1
                
    imageNum = 0
    percent = 0
    
    zeroCount = 0
    '''
    for image in trainLabels: 
        print("stage 1")
        print(imageNum)
        percent = percent+1
        if image == 0:
            #flipped vert
            aug = cv2.flip(trainImages[imageNum],0) 
            trainImages.append(aug)
            zeroCount = zeroCount + 1
        imageNum = imageNum + 1
        
    
    imageNum = 0
    for image in trainLabels:
        print("stage 2")
        print(imageNum)
        if image == 0:
            #flipped vert and horizontal
            aug = cv2.flip(trainImages[imageNum],0) 
            aug = cv2.flip(aug,1)
            trainImages.append(aug)
            zeroCount = zeroCount + 1
        imageNum = imageNum + 1
        
    imageNum = 0
    percent = 0
    for image in trainLabels:
        print("stage 3")
        print(imageNum)
        if image == 0:
            #flipped horizontal
            aug = cv2.flip(trainImages[imageNum],1)
            trainImages.append(aug)
            zeroCount = zeroCount + 1
        imageNum = imageNum + 1
        
        
    imageNum = 0
    percent = 0
    for image in trainLabels:
        print("stage 4")
        print(imageNum)
        if image == 0:
            #rotated 90
            (h,w)= trainImages[imageNum].shape[:2]
            (centerX, centerY) = (w // 2, h // 2)
            matrix = cv2.getRotationMatrix2D((centerX, centerY), 90, 1.0)
            aug = cv2.warpAffine(trainImages[imageNum], matrix, (w, h))
            trainImages.append(aug)
            zeroCount = zeroCount + 1
        imageNum = imageNum + 1
        
    imageNum = 0
    percent = 0
    for image in trainLabels:
        print("stage 5")
        print(imageNum)
        if image == 0:
            #rotates 180
            (h,w)= trainImages[imageNum].shape[:2]
            (centerX, centerY) = (w // 2, h // 2)
            matrix = cv2.getRotationMatrix2D((centerX, centerY), 180, 1.0)
            aug = cv2.warpAffine(trainImages[imageNum], matrix, (w, h))
            trainImages.append(aug)
            zeroCount = zeroCount + 1
        imageNum = imageNum + 1
        
    imageNum = 0
    percent = 0
    for image in trainLabels:
        print("stage 6")
        print(imageNum)
        if image == 0:
            #rotated 270
            (h,w)= trainImages[imageNum].shape[:2]
            (centerX, centerY) = (w // 2, h // 2)
            matrix = cv2.getRotationMatrix2D((centerX, centerY), 270, 1.0)
            aug = cv2.warpAffine(trainImages[imageNum], matrix, (w, h))
            trainImages.append(aug)
            zeroCount = zeroCount + 1
        imageNum = imageNum + 1
        
    imageNum = 0
    percent = 0
    for image in trainLabels:
        print("stage 7")
        print(imageNum)
        if image == 0:
            #rotated 270  and flipped
            (h,w)= trainImages[imageNum].shape[:2]
            (centerX, centerY) = (w // 2, h // 2)
            matrix = cv2.getRotationMatrix2D((centerX, centerY), 270, 1.0)
            aug = cv2.warpAffine(trainImages[imageNum], matrix, (w, h))
            aug = cv2.flip(aug,1)
            trainImages.append(aug)
            zeroCount = zeroCount + 1
        imageNum = imageNum + 1
        
    imageNum = 0
    percent = 0
    for image in trainLabels:
        print("stage 8")
        print(imageNum)
        percent = percent+1
        if image == 0:
            #rotated 90  and flipped
            (h,w)= trainImages[imageNum].shape[:2]
            (centerX, centerY) = (w // 2, h // 2)
            matrix = cv2.getRotationMatrix2D((centerX, centerY), 90, 1.0)
            aug = cv2.warpAffine(trainImages[imageNum], matrix, (w, h))
            aug = cv2.flip(aug,1)
            trainImages.append(aug)
            zeroCount = zeroCount + 1
        imageNum = imageNum + 1
        

    for i in range(zeroCount):
        trainLabels.append(0)
    '''    
    
    #tests
    print(len(trainImages))#9059 + (956*8) = 16707
    print(len(trainLabels))
    print(tumourCount) # 9059
    print(nTumourCount) #956 * 8
    
    
        
    return trainImages,trainLabels

def labelImages():
    return
def resizeImg(img):
    return img

def showImage(img):
    #showsImage, imgNum = imgNum
    test = hairRemoval(img)
    gray = cv2.cvtColor(test,cv2.COLOR_BGR2GRAY)
    #cv2.imshow("test Image", test)
    median = cv2.medianBlur(gray, 9)
    ret, th = cv2.threshold(median,0,255,cv2.THRESH_OTSU) #thresholding


    #tests
    kernel = np.ones((14,14),np.uint8) #kernel
    #erosion = cv2.erode(th,kernel,iterations=3) #erodes image
    #dilation = cv2.dilate(erosion,kernel, iterations=3) #dilation
    #opening = cv2.morphologyEx(th,cv2.MORPH_OPEN,kernel) #erosion + dilation
    #morph = cv2.morphologyEx(th, cv2.MORPH_GRADIENT, kernel)
    
    
    
    #close
    close = cv2.morphologyEx(th,cv2.MORPH_CLOSE,kernel)
    
    #cv2.imshow("Original Image", img)
    #cv2.imshow("Median Image",median)
    #cv2.imshow("OTSU Image",th) #median threshold
    #cv2.imshow("Erode/Dilated Image", dilation)
    #cv2.imshow("mroph", morph)
    #cv2.imshow("close",close)
    #cv2.imshow("Open Image", opening)
    
    
    #finding contours
    closeInv = cv2.bitwise_not(close) #inverts image
    result = np.zeros_like(closeInv)
    overlayedMask = overlayMask(img, result,closeInv) #overlays mask over original image
    return overlayedMask


def overlayMask(img,result,closeInv):


    # finding contours
    contours, hierachy = cv2.findContours(closeInv, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # draws largest contour
    bContour = max(contours, key=cv2.contourArea)
    cv2.drawContours(result, [bContour], -1, (255, 255, 255), -1)
    # removing noise
    result = cv2.medianBlur(result, 9)
    result = cv2.medianBlur(result, 9)

    #cv2.imshow("overlay mask",result) #shows non rotated mask

    mask = cv2.bitwise_and(img, img, mask=result)
    return mask

    #cv2.imshow("overlay mask2 ",mask)
    


def loopDirectory(dir):
    directory = os.fsencode(dir)
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".jpg"):
            #sys.stdout.write(filename)
            continue
        else:
            continue
def expandImg(img):
    top = bottom = left = right = 1280
    dst = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, (0, 0, 0))
    #cv2.imshow("boxExpanded", dst)
    return dst


def sym(img,cnt):
    #analyses symmetry
    flip = 0
    rect = cv2.minAreaRect(cnt)
    center = rect[0]
    xyLen = rect[1]
    angle = rect[2]
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    rgb = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
    cv2.drawContours(rgb,[box],0,(0,0,0),2)
    cropped = cv2.getRectSubPix(rgb,(int(xyLen[0]),int(xyLen[1])),center)
    flipx = 0
    flipy = 0
    if angle<-45:
        angle= angle+90
        flipx = xyLen[1]
        flipy = xyLen[0]
    else:
        flipx =  xyLen[0]
        flipy = xyLen[1]


    flipM = cv2.getRotationMatrix2D(center, angle, 1)
    (h, w) = rgb.shape[:2]
    rotated = cv2.warpAffine(rgb, flipM, (w, h)) #rotates to straight
    croppedRot = cv2.getRectSubPix(rotated,(int(flipx),int(flipy)),center) #crops image

    #cv2.imshow("cropped", croppedRot) #dusplays cropped image

    croppedRot = cv2.cvtColor(croppedRot, cv2.COLOR_BGR2GRAY)

    #create clone
    croppedClone = croppedRot
    (h, w) = croppedClone.shape[:2]
    if w>h:
        flip = 0
    else:
        flip = 1
    flippedClone = cv2.flip(croppedClone,flip)
    a = cv2.bitwise_or(croppedRot,flippedClone) #Lesion union Symmetry mask
    fs = cv2.bitwise_xor(croppedRot,a) #false symmetry
    #cv2.imshow("asym", fs)
    #sym = 1 - (fs/a)
    #Formula taken from "AUTOMATIC BOUNDARY DETECTION AND SYMMETRY CALCULATION IN DEMOSCOPY IMAGES OF SKIN LESIONS"
    aCount = cv2.countNonZero(a)#counts white pixels
    fsCount = cv2.countNonZero(fs)#counts white pixels
    symmetry = 1-(fsCount/aCount)

    return symmetry



    


def hairRemoval(img):

    
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    #cv2.imshow("edges",gray)
    kernel = np.ones((10,10),np.uint8)
    blackhat = cv2.morphologyEx(gray,cv2.MORPH_BLACKHAT, kernel)
    #cv2.imshow("edges2",blackhat)
    ret, th = cv2.threshold(blackhat,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    #cv2.imshow("thresh",th)
    output = cv2.inpaint(img,th,1,cv2.INPAINT_TELEA)
    #cv2.imshow("lines thing",output)
    
    #output = hairRemoval(output)
    
    return output
    
    

def shape(img):
    #analyses shape
    print("hi")


flagE = 0 #if file exists
#showImage("0024308")
if(os.path.exists("preppedData/maskOverlay/trainDataNoAug.npy")):
    flagE = 1
if flagE == 0:
    trainingImg, trainingLabels = prepTrainImages()
    trainingLabels = np.array(trainingLabels)
    trainingImg2 = np.array(trainingImg)
    trainingImg2 = np.array(trainingImg2).reshape(-1,75,100,3)
    trainingImg2, trainingLabels = shuffle(trainingImg2,trainingLabels)
    imgTrain = trainingImg2[:9014]
    imgLabel = trainingLabels[:9014]
    imgTest = trainingImg2[9014:]
    imgTestLabel = trainingLabels[9014:]
    np.save('preppedData/maskOverlay/trainDataNoAug', imgTrain)
    np.save('preppedData/maskOverlay/trainLabelNoAug',imgLabel)
    np.save('preppedData/maskOverlay/testDataNoAug', imgTest)
    np.save('preppedData/maskOverlay/testLabelNoAug',imgTestLabel)
else:
    imgTrain = np.load('preppedData/maskOverlay/trainDataNoAug.npy')
    imgLabel = np.load('preppedData/maskOverlay/trainLabelNoAug.npy')
    imgTest = np.load('preppedData/maskOverlay/testDataNoAug.npy')
    imgTestLabel = np.load('preppedData/maskOverlay/testLabelNoAug.npy')
#showImage("0024308")





datagen = ImageDataGenerator()
# prepare an iterators to scale images

imgTrain2 = []
for image in imgTrain:
    #print(count/len(trainingImg2)*100)
    #count+=1
    b = image.astype(float)/255
    imgTrain2.append(b)
imgTrain2 = np.array(imgTrain2)
datagen.fit(imgTrain2)

imgTest2 = []
for image in imgTest:
    #print(count/len(trainingImg2)*100)
    #count+=1
    b = image.astype(float)/255
    imgTest2.append(b)
imgTest2 = np.array(imgTest2)
datagen.fit(imgTest2)
#datagen.fit(imgTest)
trIterator = datagen.flow(imgTrain2, imgLabel, batch_size=64)
teIterator = datagen.flow(imgTest2, imgTestLabel, batch_size=64)
print('Batches train=%d, test=%d' % (len(trIterator), len(teIterator)))
# confirm the scaling works
batchX, batchy = trIterator.next()
print('Batch shape=%s, min=%.3f, max=%.3f' % (batchX.shape, batchX.min(), batchX.max()))


#save these arrays somehow


model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(75, 100, 3)))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))

model.summary()

model.add(layers.Flatten())
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(2,activation = "softmax"))



model.summary()
opt = tf.keras.optimizers.Adam(learning_rate=0.001)
model.compile(optimizer=opt,
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False),
              metrics=['accuracy'])
checkpoint = ModelCheckpoint(filepath="modelOMCNN", monitor="val_loss",  verbose=1, save_best_only=True, mode="min")
callbacks = checkpoint
#history = model.fit(trIterator,steps_per_epoch=len(trIterator), epochs=16,verbose = 1)
#history = model.fit(trIterator,steps_per_epoch=len(trIterator), epochs=16,verbose = 1)
history = model.fit(trIterator,steps_per_epoch=len(imgTrain)//64,validation_data = teIterator,
                    validation_steps = (len(imgTest2))//64, epochs=16,verbose = 1,callbacks = callbacks)
'''
pyplot.plot(history.history['accuracy'], label='accuracy')
pyplot.plot(history.history['val_accuracy'], label = 'val_accuracy')
pyplot.xlabel('Epoch')
pyplot.ylabel('Accuracy')
pyplot.ylim([0.5, 1])
pyplot.legend(loc='lower right')
'''
model.save('saved/myModelOverlay')
predictions = model.predict(imgTest2,batch_size = 64, verbose = 1)
predictionValues = []

for predicted in predictions:
    tick = 0
    count = 0
    for value in predicted:
        if (1-(1-value))>0.5:
            predictionValues.append(count)
            tick = 1
        count += 1
    if tick == 0:
        print("error")
        print(predicted)
predictionValues = np.array(predictionValues)        
print(tf.math.confusion_matrix(imgTestLabel,predictionValues))
test_loss, test_acc = model.evaluate(teIterator, steps = len(teIterator), verbose=1)





print('Restored model, accuracy: {:5.2f}%'.format(100 * test_acc))