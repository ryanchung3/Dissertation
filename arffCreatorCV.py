import cv2
import numpy as np
import os
import sys
import math
from PIL import Image  
import PIL  
from matplotlib import pyplot
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
import skimage
from scipy.stats import skew, entropy


def createArff():
    flagE = 0
    if(os.path.exists("preppedData/traditional/symmetryList.npy")):
        flagE = 1
    
    if flagE == 0:
        print("error: no values found")
    else:
        symmetryList = np.load('preppedData/traditional/symmetryList.npy')
        homogenyList = np.load('preppedData/traditional/homogenyList.npy')
        energyList = np.load('preppedData/traditional/energyList.npy')
        correlationList = np.load('preppedData/traditional/correlationList.npy')
        areaList = np.load('preppedData/traditional/areaList.npy')
        perimeterList = np.load('preppedData/traditional/perimeterList.npy')
        convexList = np.load('preppedData/traditional/convexList.npy')
        ecenList = np.load('preppedData/traditional/ecenList.npy')
        compactList = np.load('preppedData/traditional/compactList.npy')
        roundnessList = np.load('preppedData/traditional/roundnessList.npy')
        convexValList = np.load('preppedData/traditional/convexValList.npy')
        ageList = np.load('preppedData/traditional/ageList.npy')
        sexList= np.load('preppedData/traditional/sexList.npy')
        labelList= np.load('preppedData/traditional/labelList.npy')
        
        
        
        
        
        
        
        
    minL = np.load('preppedData/traditional/minL.npy')
    minA= np.load('preppedData/traditional/minA.npy')
    minB= np.load('preppedData/traditional/minB.npy')
    
    maxL= np.load('preppedData/traditional/maxL.npy')
    maxA= np.load('preppedData/traditional/maxA.npy')
    maxB= np.load('preppedData/traditional/maxB.npy')
    
    meanL= np.load('preppedData/traditional/meanL.npy')
    meanA= np.load('preppedData/traditional/meanA.npy')
    meanB= np.load('preppedData/traditional/meanB.npy')
    
    sdL= np.load('preppedData/traditional/sdL.npy')
    sdA= np.load('preppedData/traditional/sdA.npy')
    sdB= np.load('preppedData/traditional/sdB.npy')
    
    skewL= np.load('preppedData/traditional/skewL.npy')
    skewA= np.load('preppedData/traditional/skewA.npy')
    skewB= np.load('preppedData/traditional/skewB.npy')
    
    entL= np.load('preppedData/traditional/entL.npy')
    entA= np.load('preppedData/traditional/entA.npy')
    entB= np.load('preppedData/traditional/entB.npy')
    
    varL= np.load('preppedData/traditional/varL.npy')
    varA= np.load('preppedData/traditional/varA.npy')
    varB= np.load('preppedData/traditional/varB.npy')
    
    
    
    
    
    minR = np.load('preppedData/traditional/minR.npy')
    minG= np.load('preppedData/traditional/minG.npy')
    minBl= np.load('preppedData/traditional/minBl.npy')
    
    maxR = np.load('preppedData/traditional/maxR.npy')
    maxG= np.load('preppedData/traditional/maxG.npy',)
    maxBl= np.load('preppedData/traditional/maxBl.npy')
    
    meanR= np.load('preppedData/traditional/meanR.npy')
    meanG= np.load('preppedData/traditional/meanG.npy')
    meanBl= np.load('preppedData/traditional/meanBl.npy')
    
    sdR= np.load('preppedData/traditional/sdR.npy')
    sdG= np.load('preppedData/traditional/sdG.npy')
    sdBl= np.load('preppedData/traditional/sdBl.npy')
    
    skewR= np.load('preppedData/traditional/skewR.npy')
    skewG= np.load('preppedData/traditional/skewG.npy')
    skewBl= np.load('preppedData/traditional/skewBl.npy')
    
    entR= np.load('preppedData/traditional/entR.npy')
    entG =np.load('preppedData/traditional/entG.npy')
    entBl= np.load('preppedData/traditional/entBl.npy')
    
    varR= np.load('preppedData/traditional/varR.npy')
    varG= np.load('preppedData/traditional/varG.npy')
    varBl= np.load('preppedData/traditional/varBl.npy')
    
    
    
    minH= np.load('preppedData/traditional/minH.npy')
    minS= np.load('preppedData/traditional/minS.npy')
    minV= np.load('preppedData/traditional/minV.npy')
    
    maxH= np.load('preppedData/traditional/maxH.npy')
    maxS= np.load('preppedData/traditional/maxS.npy')
    maxV= np.load('preppedData/traditional/maxV.npy')
    
    meanH= np.load('preppedData/traditional/meanH.npy')
    meanS= np.load('preppedData/traditional/meanS.npy')
    meanV= np.load('preppedData/traditional/meanV.npy')
    
    sdH= np.load('preppedData/traditional/sdH.npy')
    sdS= np.load('preppedData/traditional/sdS.npy')
    sdV = np.load('preppedData/traditional/sdV.npy')
    
    skewH= np.load('preppedData/traditional/skewH.npy')
    skewS= np.load('preppedData/traditional/skewS.npy')
    skewV= np.load('preppedData/traditional/skewV.npy')
    
    entH= np.load('preppedData/traditional/entH.npy')
    entS= np.load('preppedData/traditional/entS.npy')
    entV= np.load('preppedData/traditional/entV.npy')
    
    varH= np.load('preppedData/traditional/varH.npy')
    varS= np.load('preppedData/traditional/varS.npy')
    varV= np.load('preppedData/traditional/varV.npy')
        
        
    file = open("lesion.arff","w")
    file.write("@RELATION skinLesions\n")
    file.write("@ATTRIBUTE symmetry          NUMERIC\n")
    file.write("@ATTRIBUTE homogeny          NUMERIC\n")
    file.write("@ATTRIBUTE energy            NUMERIC\n")
    file.write("@ATTRIBUTE correlation       NUMERIC\n")
    file.write("@ATTRIBUTE normArea          NUMERIC\n")
    file.write("@ATTRIBUTE normPerimeter     NUMERIC\n")
    file.write("@ATTRIBUTE normConvex        NUMERIC\n")
    file.write("@ATTRIBUTE eccentricity      NUMERIC\n")
    file.write("@ATTRIBUTE compactness       NUMERIC\n")
    file.write("@ATTRIBUTE roundness         NUMERIC\n")
    file.write("@ATTRIBUTE convexity         NUMERIC\n")
    file.write("@ATTRIBUTE age               NUMERIC\n")
    file.write("@ATTRIBUTE sex               NUMERIC\n")
    
    file.write("@ATTRIBUTE minL              NUMERIC\n")
    file.write("@ATTRIBUTE minA              NUMERIC\n")
    file.write("@ATTRIBUTE minB              NUMERIC\n")
    file.write("@ATTRIBUTE maxL              NUMERIC\n")
    file.write("@ATTRIBUTE maxA              NUMERIC\n")
    file.write("@ATTRIBUTE maxB              NUMERIC\n")
    file.write("@ATTRIBUTE meanL             NUMERIC\n")
    file.write("@ATTRIBUTE meanA             NUMERIC\n")
    file.write("@ATTRIBUTE meanB             NUMERIC\n")
    file.write("@ATTRIBUTE sdL               NUMERIC\n")
    file.write("@ATTRIBUTE sdA               NUMERIC\n")
    file.write("@ATTRIBUTE sdB               NUMERIC\n")
    file.write("@ATTRIBUTE skewL             NUMERIC\n")
    file.write("@ATTRIBUTE skewA             NUMERIC\n")
    file.write("@ATTRIBUTE skewB             NUMERIC\n")
    file.write("@ATTRIBUTE varL              NUMERIC\n")
    file.write("@ATTRIBUTE varA              NUMERIC\n")
    file.write("@ATTRIBUTE varB              NUMERIC\n")
    
    
    file.write("@ATTRIBUTE minR              NUMERIC\n")
    file.write("@ATTRIBUTE minG              NUMERIC\n")
    file.write("@ATTRIBUTE minBl             NUMERIC\n")
    file.write("@ATTRIBUTE maxR              NUMERIC\n")
    file.write("@ATTRIBUTE maxG              NUMERIC\n")
    file.write("@ATTRIBUTE maxBl             NUMERIC\n")
    file.write("@ATTRIBUTE meanR             NUMERIC\n")
    file.write("@ATTRIBUTE meanG             NUMERIC\n")
    file.write("@ATTRIBUTE meanBl            NUMERIC\n")
    file.write("@ATTRIBUTE sdR               NUMERIC\n")
    file.write("@ATTRIBUTE sdG               NUMERIC\n")
    file.write("@ATTRIBUTE sdBl              NUMERIC\n")
    file.write("@ATTRIBUTE skewR             NUMERIC\n")
    file.write("@ATTRIBUTE skewG             NUMERIC\n")
    file.write("@ATTRIBUTE skewBl            NUMERIC\n")
    file.write("@ATTRIBUTE varR              NUMERIC\n")
    file.write("@ATTRIBUTE varG              NUMERIC\n")
    file.write("@ATTRIBUTE varBl             NUMERIC\n")
    
    
    file.write("@ATTRIBUTE minH              NUMERIC\n")
    file.write("@ATTRIBUTE minS              NUMERIC\n")
    file.write("@ATTRIBUTE minV              NUMERIC\n")
    file.write("@ATTRIBUTE maxH              NUMERIC\n")
    file.write("@ATTRIBUTE maxS              NUMERIC\n")
    file.write("@ATTRIBUTE maxV              NUMERIC\n")
    file.write("@ATTRIBUTE meanH             NUMERIC\n")
    file.write("@ATTRIBUTE meanS             NUMERIC\n")
    file.write("@ATTRIBUTE meanV             NUMERIC\n")
    file.write("@ATTRIBUTE sdH               NUMERIC\n")
    file.write("@ATTRIBUTE sdS               NUMERIC\n")
    file.write("@ATTRIBUTE sdV               NUMERIC\n")
    file.write("@ATTRIBUTE skewH             NUMERIC\n")
    file.write("@ATTRIBUTE skewS             NUMERIC\n")
    file.write("@ATTRIBUTE skewV             NUMERIC\n")
    file.write("@ATTRIBUTE varH              NUMERIC\n")
    file.write("@ATTRIBUTE varS              NUMERIC\n")
    file.write("@ATTRIBUTE varV              NUMERIC\n")

    
    
    file.write("@ATTRIBUTE CLASS             {1.0,0.0}\n")
    file.write("@data\n")
    
    for i in range(len(labelList)):
        file.write(str(symmetryList[i]))
        file.write(",")
        file.write(str(homogenyList[i]))
        file.write(",")
        file.write(str(energyList[i]))
        file.write(",")
        file.write(str(correlationList[i]))
        file.write(",")
        file.write(str(areaList[i]))
        file.write(",")
        file.write(str(perimeterList[i]))
        file.write(",")
        
        if convexList[i] == 0:
            file.write("0.0,")
        elif convexList[i] == 1:
            file.write("1.0,")
        else:
            print("error")
            
        file.write(str(ecenList[i]))
        file.write(",")
        file.write(str(compactList[i]))
        file.write(",")
        file.write(str(roundnessList[i]))
        file.write(",")
        file.write(str(convexValList[i]))
        file.write(",")
        if ageList[i]== "":
            file.write(str(ageList[i]))
        else:
            file.write("-1")
        file.write(",")
        
        if sexList[i]== "male":
            file.write("1.0")
        elif sexList[i]=="female":
            file.write("2.0")
        else:
            file.write("0.0")
            
        
        
        file.write(",")
        file.write(str(minL[i]))
        file.write(",")
        file.write(str(minA[i]))
        file.write(",")
        file.write(str(minB[i]))
        file.write(",")
        file.write(str(maxL[i]))
        file.write(",")
        file.write(str(maxA[i]))
        file.write(",")
        file.write(str(maxB[i]))
        file.write(",")
        file.write(str(meanL[i]))
        file.write(",")
        file.write(str(meanA[i]))
        file.write(",")
        file.write(str(meanB[i]))
        file.write(",")
        file.write(str(sdL[i]))
        file.write(",")
        file.write(str(sdA[i]))
        file.write(",")
        file.write(str(sdB[i]))
        file.write(",")
        file.write(str(skewL[i]))
        file.write(",")
        file.write(str(skewA[i]))
        file.write(",")
        file.write(str(skewB[i]))
        file.write(",")
        file.write(str(varL[i]))
        file.write(",")
        file.write(str(varA[i]))
        file.write(",")
        file.write(str(varB[i]))
        file.write(",")
        
        
        file.write(str(minR[i]))
        file.write(",")
        file.write(str(minG[i]))
        file.write(",")
        file.write(str(minBl[i]))
        file.write(",")
        file.write(str(maxR[i]))
        file.write(",")
        file.write(str(maxG[i]))
        file.write(",")
        file.write(str(maxBl[i]))
        file.write(",")
        file.write(str(meanR[i]))
        file.write(",")
        file.write(str(meanG[i]))
        file.write(",")
        file.write(str(meanBl[i]))
        file.write(",")
        file.write(str(sdR[i]))
        file.write(",")
        file.write(str(sdG[i]))
        file.write(",")
        file.write(str(sdBl[i]))
        file.write(",")
        file.write(str(skewR[i]))
        file.write(",")
        file.write(str(skewG[i]))
        file.write(",")
        file.write(str(skewBl[i]))
        file.write(",")
        file.write(str(varR[i]))
        file.write(",")
        file.write(str(varG[i]))
        file.write(",")
        file.write(str(varBl[i]))
        file.write(",")
        
        file.write(str(minH[i]))
        file.write(",")
        file.write(str(minS[i]))
        file.write(",")
        file.write(str(minV[i]))
        file.write(",")
        file.write(str(maxH[i]))
        file.write(",")
        file.write(str(maxS[i]))
        file.write(",")
        file.write(str(maxV[i]))
        file.write(",")
        file.write(str(meanH[i]))
        file.write(",")
        file.write(str(meanS[i]))
        file.write(",")
        file.write(str(meanV[i]))
        file.write(",")
        file.write(str(sdH[i]))
        file.write(",")
        file.write(str(sdS[i]))
        file.write(",")
        file.write(str(sdV[i]))
        file.write(",")
        file.write(str(skewH[i]))
        file.write(",")
        file.write(str(skewS[i]))
        file.write(",")
        file.write(str(skewV[i]))
        file.write(",")
        file.write(str(varH[i]))
        file.write(",")
        file.write(str(varS[i]))
        file.write(",")
        file.write(str(varV[i]))
        file.write(",")
        
        if labelList[i] == 0:
            file.write("0.0\n")
        elif labelList[i] == 1:
            file.write("1.0\n")
        else:
            print("error")


createArff()
